using System;
using System.Data.SQLite;

public class Item
{
    public int itemID {get; set;}
    public string itemName {get; set;} 
    public double minStat {get; set;}
    public double maxStat {get; set;}

    public Item()
    {
        // here
    }

    public static void MakeItem(Item item, int _itemID)
    {
        item.itemID = _itemID;
        string sql = "select * from items where items.ItemID=@ID";
        SQLiteConnection gamedb = new SQLiteConnection("Data Source=gamedata.sqlite; Version=3;");
        gamedb.Open();
        using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
        {
            cmd.Prepare();
            cmd.Parameters.AddWithValue("@ID", item.itemID);
            SQLiteDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                {
                    item.itemName = (string)reader["ItemName"];
                    item.minStat = (Int64)reader["MinStat"];
                    item.maxStat = (Int64)reader["MaxStat"];
                }
        }
        gamedb.Close();
    
    }

    public static void AddItem(Player _player, double _itemID)
    {
        Player player = _player;
        double itemID = _itemID;
        string sql = sql = "insert into inventory (PlayerID, ItemID) values (@PlayerID, @ItemID)";
        SQLiteConnection gamedb = new SQLiteConnection("Data Source=gamedata.sqlite; Version=3;");
        gamedb.Open();
        using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
        {
            cmd.Prepare();
            cmd.Parameters.AddWithValue("@PlayerID", player.playerID);
            cmd.Parameters.AddWithValue("@ItemID", itemID);
            cmd.ExecuteNonQuery();
        }
        gamedb.Close();
    }

    public static void DisplayInventory(Player _player)
    {
        Player player = _player;
        string sql = "select * from inventory join items on inventory.ItemID = items.ItemID where PlayerID = @PlayerID";
        SQLiteConnection gamedb = new SQLiteConnection("Data Source=gamedata.sqlite; Version=3;");
        gamedb.Open();
        Console.WriteLine("Inventory: ");
        Console.WriteLine("{0} the {1}", player.playerName, player.playerClass);
        Console.WriteLine();
        using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
        {
            cmd.Prepare();
            cmd.Parameters.AddWithValue("@PlayerID", player.playerID);
            SQLiteDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                {
                    Console.WriteLine("Item: " + reader["ItemName"]);
                }
        }
        gamedb.Close();
        Console.WriteLine();
        Console.WriteLine("Gold: {0}", player.playerGold);
        Console.WriteLine();
    }

    public static void UseItem(Player _player)
    {
        Player player = _player;
        string response = "";
        double choice=0;
        double restore = 0;
        string sql = "select * from inventory join items on inventory.ItemID = items.ItemID where PlayerID = @PlayerID and ItemType = @ItemType";
        Random r = new Random();
        SQLiteConnection gamedb = new SQLiteConnection("Data Source=gamedata.sqlite; Version=3;");
        
        Console.WriteLine("Use an item:");
        Console.WriteLine("1. Health Potion");
        Console.WriteLine("2. Mana Potion");
        Console.WriteLine("3. Poison");
        Console.WriteLine();
        Console.Write("Enter your selection (1-3): ");
        response = Console.ReadLine();
        
        switch (response)
        {
            case "1":
                gamedb.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
                {
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@PlayerID", player.playerID);
                    cmd.Parameters.AddWithValue("@ItemType", "potion1");
                    SQLiteDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                        {
                            Console.WriteLine("{0}: {1}", reader["InventoryID"], reader["ItemName"]);
                        }
                }
                gamedb.Close();
                Console.Write("Enter potion ID (0 to exit): ");
                choice = Convert.ToInt32(Console.ReadLine());
                if (choice == 0)
                {
                    break;
                }
                sql = "select * from inventory join items on inventory.ItemID = items.ItemID where inventory.InventoryID = @InventoryID";
                gamedb.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
                {
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@InventoryID", choice);
                    SQLiteDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                        {
                            double min = (Int64)reader["MinStat"];
                            double max = (Int64)reader["MaxStat"];
                            restore = r.Next((int)min,(int)max);
                            player.playerHP += restore;
                            Console.WriteLine("+{0} HP restored", restore);
                            Console.WriteLine("You have {0} HP.", player.playerHP);
                        }
                }
                gamedb.Close();
                break;
            case "2":
                gamedb.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
                {
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@PlayerID", player.playerID);
                    cmd.Parameters.AddWithValue("@ItemType", "potion2");
                    SQLiteDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                        {
                            Console.WriteLine("{0}: {1}", reader["InventoryID"], reader["ItemName"]);
                        }
                }
                gamedb.Close();
                Console.Write("Enter potion ID (0 to exit): ");
                choice = Convert.ToInt32(Console.ReadLine());
                if (choice == 0)
                {
                    break;
                }
                sql = "select * from inventory join items on inventory.ItemID = items.ItemID where inventory.InventoryID = @InventoryID";
                gamedb.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
                {
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@InventoryID", choice);
                    SQLiteDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                        {
                            double min = (Int64)reader["MinStat"];
                            double max = (Int64)reader["MaxStat"];
                            restore = r.Next((int)min,(int)max);
                            player.playerMP += restore;
                            Console.WriteLine("+{0} MP restored", restore);
                            Console.WriteLine("You have {0} MP.", player.playerMP);
                        }
                }
                gamedb.Close();
                break;
            case "3":
                gamedb.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
                {
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@PlayerID", player.playerID);
                    cmd.Parameters.AddWithValue("@ItemType", "potion1");
                    SQLiteDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                        {
                            Console.WriteLine("{0}: {1}", reader["InventoryID"], reader["ItemName"]);
                        }
                }
                gamedb.Close();
                Console.Write("Enter potion ID (0 to exit): ");
                choice = Convert.ToInt32(Console.ReadLine());
                if (choice == 0)
                {
                    break;
                }
                sql = "select * from inventory join items on inventory.ItemID = items.ItemID where inventory.InventoryID = @InventoryID";
                gamedb.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
                {
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@InventoryID", choice);
                    SQLiteDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                        {
                            double min = (Int64)reader["MinStat"];
                            double max = (Int64)reader["MaxStat"];
                            restore = r.Next((int)min,(int)max);
                            player.attackBonus += restore;
                            Console.WriteLine("+{0} attack bonus for this battle", restore);
                        }
                }
                gamedb.Close();
                break;
        }
        gamedb.Close();
        sql = "delete from inventory where InventoryID = @InventoryID";
        gamedb.Open();
        using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
        {
            cmd.Prepare();
            cmd.Parameters.AddWithValue("@InventoryID", choice);
            cmd.ExecuteNonQuery();
        }
        gamedb.Close();
    }
}
