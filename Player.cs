using System;
using System.Data.SQLite;

public class Player
{
    public double playerID {get; set;}
    public string playerName {get; set;} 
    public string playerClass {get; set;}
    public string playerWeapon {get; set;}
    public Item activeWeapon {get; set;} 
    public double playerStrength {get; set;}
    public double playerMysticism {get; set;}
    public double playerDexterity {get; set;}
    public double playerDefense {get; set;}
    public double playerLevel {get; set;}
    public double playerHP {get; set;}
    public double playerMP {get; set;}
    public double playerXP {get; set;}
    public int playerGold {get; set;}
    public int[,] spellbook {get; set;}
    public double attackBonus {get; set;}
    
    public Player()
    {
        
    }
    public void DisplayStats()
    {
        Console.WriteLine("{0} the {1}", playerName, playerClass);
        Console.WriteLine("Level: " + (int)Math.Ceiling(Math.Pow(playerXP/8, 0.3)));
        Console.WriteLine("XP: " + (int)Math.Ceiling(playerXP));
        Console.WriteLine("Strength: " + (int)Math.Ceiling(playerStrength));
        Console.WriteLine("Mysticism: " + (int)Math.Ceiling(playerMysticism));
        Console.WriteLine("Dexterity: " + (int)Math.Ceiling(playerDexterity));
        Console.WriteLine("Defense: " + (int)Math.Ceiling(playerDefense));
        Console.WriteLine("HP: " + (int)(playerHP));
        Console.WriteLine("MP: " + (int)(playerMP));
        Console.WriteLine();
    }

    public void GenerateStats()
    {
        Random r = new Random();    
        playerLevel = Math.Pow(playerXP/8, 0.3);
        spellbook = new int[5,2];
        spellbook[0,0] = 1;
        spellbook[1,0] = 2;
        spellbook[2,0] = 3;
        spellbook[3,0] = 4;
        spellbook[4,0] = 5;
        switch (playerWeapon)
        {
            case "sword":
            playerClass = "fighter";
            playerStrength = r.Next(16, 20);
            playerMysticism = r.Next(13, 20);
            playerDexterity = r.Next(11, 20);
            playerDefense = r.Next(15, 20);
            playerHP =Math.Ceiling(playerStrength + (playerLevel * Math.Pow(playerStrength*playerDefense, 0.5)));
            playerMP =Math.Ceiling(playerMysticism + (playerLevel * Math.Pow(playerMysticism, 0.5)));
            spellbook[0,1] = 2;
            break;

            case "staff":
            playerClass = "mage";
            playerStrength = r.Next(11, 20);
            playerMysticism = r.Next(16, 20);
            playerDexterity = r.Next(13, 20);
            playerDefense = r.Next(12, 20);
            playerHP =Math.Ceiling(playerStrength + (playerLevel * Math.Pow(playerStrength*playerDefense, 0.5)));
            playerMP =Math.Ceiling(playerMysticism + (playerLevel * Math.Pow(playerMysticism, 0.5)));
            spellbook[0,1] = 1;
            break;

            case "bow":
            playerClass = "rogue";
            playerStrength = r.Next(13, 20);
            playerMysticism = r.Next(11, 20);
            playerDexterity = r.Next(16, 20);
            playerDefense = r.Next(13,20);
            playerHP =Math.Ceiling(playerStrength + (playerLevel * Math.Pow(playerStrength* playerDefense, 0.5)));
            playerMP =Math.Ceiling(playerMysticism + (playerLevel * Math.Pow(playerMysticism, 0.5)));
            spellbook[0,1] = 3;
            break;
        }
        string sql = "INSERT INTO players(PlayerName, Class, Level, XP, Strength, Mysticism, Dexterity, Defense, HP, MP, Gold) VALUES (@PlayerName, @Class, @Level, @XP, @Strength, @Mysticism, @Dexterity, @Defense, @HP, @MP, @Gold)";
        SQLiteConnection gamedb = new SQLiteConnection("Data Source=gamedata.sqlite; Version=3;");
        gamedb.Open();
        using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
        {
            cmd.Prepare();
            cmd.Parameters.AddWithValue("@PlayerName", playerName);
            cmd.Parameters.AddWithValue("@Level", playerLevel);
            cmd.Parameters.AddWithValue("@Class", playerClass);
            cmd.Parameters.AddWithValue("@XP", playerXP);
            cmd.Parameters.AddWithValue("@Strength", playerStrength);
            cmd.Parameters.AddWithValue("@Mysticism", playerMysticism);
            cmd.Parameters.AddWithValue("@Dexterity", playerDexterity);
            cmd.Parameters.AddWithValue("@Defense", playerDefense);
            cmd.Parameters.AddWithValue("@HP", playerHP);
            cmd.Parameters.AddWithValue("@MP", playerMP);
            cmd.Parameters.AddWithValue("@Gold", playerGold);
            cmd.ExecuteNonQuery();
        }
        gamedb.Close();
        gamedb.Open();
        sql = "select * from players where PlayerName= @PlayerName";
        using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
        {
            cmd.Prepare();
            cmd.Parameters.AddWithValue("@PlayerName", playerName);
            SQLiteDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                {
                    playerID = (Int64)reader["PlayerID"];
                }
        }
        gamedb.Close();
    }

    public void DisplaySpellbook()
    {
        string sql = "select * from spells where spellID = @spellID";
        SQLiteConnection gamedb = new SQLiteConnection("Data Source=gamedata.sqlite; Version=3;");
        gamedb.Open();
        
        for (int x = 0; x < 5; x++)
        {
            using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
            {
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@spellID", spellbook[x,1]);
                SQLiteDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                {
                    Console.WriteLine("{0}. {1}", x+1, reader["SpellName"]);
                }
            }
        }
        gamedb.Close();
    }
}

