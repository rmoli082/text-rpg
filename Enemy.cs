using System;
using System.Collections.Generic;
using System.Data.SQLite;

public class Enemy
{
    public double enemyID {get; set;}
    public string enemyName {get; set;} 
    public double enemyAttack {get; set;} 
    public double minStr {get; set;}
    public double minMys {get; set;}
    public double minDex {get; set;}
    public double minDef {get; set;}
    public double maxStr {get; set;}
    public double maxMys {get; set;}
    public double maxDex {get; set;}
    public double maxDef {get; set;}
    public double enemyLevel {get; set;}
    public double enemyStrength {get; set;}
    public double enemyMysticism {get; set;}
    public double enemyDexterity {get; set;}
    public double enemyDefense {get; set;}
    public double enemyHP {get; set;}
    public double enemyMP {get; set;}
    public double enemyXP {get; set;}
    public int enemyGold {get; set;}
    public Enemy()
    {
        
    }

    public void SpawnEnemy(int _enemyID)
    {
        Random r = new Random();
        enemyID = _enemyID;
        string sql = "select * from enemies where enemies.EnemyID=@ID";
        SQLiteConnection gamedb = new SQLiteConnection("Data Source=gamedata.sqlite; Version=3;");
        gamedb.Open();
        using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
        {
            cmd.Prepare();
            cmd.Parameters.AddWithValue("@ID", enemyID);
            SQLiteDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                {
                    enemyName = (string)reader["EnemyName"];
                    enemyLevel = (Int64)reader["Level"];
                    minStr = (Int64)reader["StrengthMin"];
                    maxStr = (Int64)reader["StrengthMax"];
                    minMys = (Int64)reader["MysticismMin"];
                    maxMys = (Int64)reader["MysticismMax"];
                    minDex = (Int64)reader["DexterityMin"];
                    maxDex = (Int64)reader["DexterityMax"];
                    minDef = (Int64)reader["DefenseMin"];
                    maxDef = (Int64)reader["DefenseMax"];
                    enemyStrength = r.Next((int)minStr, (int)maxStr); 
                    enemyMysticism = r.Next((int)minMys, (int)maxMys);
                    enemyDexterity = r.Next((int)minDex, (int)maxDex);
                    enemyDefense = r.Next((int)minDef, (int)maxDef);
                    enemyAttack = Math.Ceiling(Math.Pow(enemyStrength*enemyLevel, 0.5));
                    enemyHP = Math.Ceiling(enemyDefense + (enemyLevel * Math.Pow(enemyStrength*enemyDefense, 0.5)));
                    enemyMP = Math.Ceiling(enemyMysticism + (enemyLevel * Math.Pow(enemyMysticism, 0.5)));
                    enemyXP = 5 * enemyLevel;
                    enemyGold = r.Next(0, 3*(int)enemyLevel);
                }
        }
        gamedb.Close();
    
    }

}