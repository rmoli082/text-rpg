﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace TextRPG
{
    class Program
    {
        static void Main(string[] args)
        {
            Player playerOne = new Player();

            GameOpening();
            Introduction(playerOne);
            KingsDungeons(playerOne);
        }

        public static void GameOpening()
        {
            Console.Clear();
            Console.WriteLine("Text Adventure RPG");
            Console.WriteLine();
            Console.WriteLine("A game by Robert Molinaro");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
        }

        public static void Introduction(Player _player)
        {
            Player player = _player;
            Console.Clear();
            
            Console.WriteLine("You wake up in a dimly lit dungeon. Your head hurts and your vision is blurred. You gingerly look around the cell and see a gaunt old man in tattered rags sitting with you. 'Water,' you say, your voice creaking and rusty from disuse. How long had you been out for? 'HAH!' the old man cackled gleefully. 'You won't find much water here, no siree. What is your name, child?' the old man asked");
            Console.WriteLine();
            Console.Write("Enter your name: ");
            player.playerName = Console.ReadLine();
            Console.Clear();
            Console.WriteLine("'A fine name. Very noble. Very heroic,' he snorted.");
            
            do
            {
            Console.WriteLine("'Was it the sword, bow or the staff that landed you here?' the old man asked.");
            Console.WriteLine();
            Console.Write("Enter selection: ");
            player.playerWeapon = Console.ReadLine().ToLower();
            if (player.playerWeapon == "sword" || player.playerWeapon == "staff" || player.playerWeapon == "bow")
            {
                break;
            }
            else
            {
                Console.WriteLine("Not a valid entry. Please try again...");
            }
            } while (true);
            
            player.GenerateStats();
            
            Console.WriteLine();
            Console.WriteLine("'Ahhh, so you're a {0} then. {1} the {0}. Very nice. Very powerful. Yes.' He gives you a sly look. 'Will be powerful one day. Now...'", 
            player.playerClass, player.playerName);
            Console.WriteLine();
            Console.WriteLine("Press enter to see your stats...");
            Console.ReadLine();
            Console.Clear();
            
            player.DisplayStats();
            Console.WriteLine();
            Console.WriteLine("You can check your stats at any time by issuing the 'stats' command.");
            Console.WriteLine();
            Console.WriteLine("You grimaced. He was right. But once you busted out of here...Where even were you? You had no idea what god forsaken cell you landed yourself in.");
            Console.WriteLine();
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
            Console.Clear();
            
            Console.WriteLine("'Where am I?' you ask the old man. 'I know I'm in a cell. But where?'");
            Console.WriteLine();
            Console.WriteLine("The old man pauses a moment to think. 'I'm not sure exactly. I'm not too sure.' He frowns. 'I can tell you this though. You won't find any place further away from civilization than this hell hole you've just woken up in. That's for sure. No siree.' He shudders. 'I hear them screaming all night.'");
            Console.WriteLine();
            Console.WriteLine("You get up and start to look around the cell. It's damp and dark. The ground is bare and there's blood soaked straw lining the otherwise bare ground. It was obvious that you and the old man were not the first prisoners here.");
            Console.WriteLine();
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
            Console.Clear();
            
            Console.WriteLine("'I need to get out of here,' you say almost frantically. You had never been known to be claustrophobic before, but you were now. You try the door. To your surprise it was not locked. You turn to look at the old man.");
            Console.WriteLine();
            Console.WriteLine("'Yeah I knew. I always knew. You won't be the first to try it if you go. And you won't be the last I'm sure. No you won't be. No siree.' The old man cackled again and sighed. 'But, if you go, I have some things that might help you.'");
            Console.WriteLine();
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
            Console.Clear();
            
            Item weapon = new Item();
            int itemID=0;
            switch (player.playerClass)
            {
                case "fighter":
                itemID = 1;
                break;
                case "rogue":
                itemID = 2;
                break;
                case "mage":
                itemID = 3;
                break;
            }
            Item.MakeItem(weapon, itemID);
            Console.WriteLine("'Here, take this.'");
            Console.WriteLine("The old man holds out a {0} and a small sack of gold.", weapon.itemName);
            Console.WriteLine();
            Console.WriteLine("You reach out and grab the gold and the {0}.", weapon.itemName);
            player.playerGold += 25;
            Item.AddItem(player, itemID);
            player.activeWeapon = weapon;
            Console.WriteLine("You now have {0} gold and a {1} in your inventory.", player.playerGold, 
            weapon.itemName);
            Console.WriteLine();
            Console.Write("Press enter now to display your inventory...");
            Console.ReadLine();
            Console.WriteLine();
            Console.Clear();

            Item.DisplayInventory(player);
            Console.WriteLine();
            Console.WriteLine("You can view your inventory at any time by issuing the 'inventory' command.");
            Console.WriteLine();
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
            Console.Clear();
            
            Console.WriteLine("'Well, my boy, I know it isn't much. No siree it isn't. But it'll help. It'll help. You can count on that.' He cackled again and you smiled. You were going to miss the old man despite yourself.");
            Console.WriteLine();
            Console.WriteLine("You grab the doorhandle again, and as you pull the door open, you hear the old man cackling again. Slowly it starts to fade away. You turn and look and the old man is gone...");
            Console.WriteLine();
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
            Console.Clear();
        }

        public static void KingsDungeons(Player _player)
        {
            Player player = _player;
            Enemy guard = new Enemy();
            guard.SpawnEnemy(1);
            Console.WriteLine("You step out of the cell into a long, dark corridor filled with cells like the one you just left. You slowly start to walk down the hall, peering into some of the cells as you go along. Some of them are empty. Most are full. The inhabitants are bruised, bloody and starved.");
            Console.WriteLine();
            Console.WriteLine("Up ahead, you see a {0} standing. He hasn't noticed you yet so you pull out your {1} and get ready to fight. The {0} notices you and he pulls out his sword and charges at you.", guard.enemyName, player.playerWeapon);
            Console.WriteLine();
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
            Console.Clear();
            Fight(player, guard);
            Console.WriteLine();
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
            Console.Clear();
            player.DisplayStats();
            Console.ReadLine();
            Console.Clear();
            Item.DisplayInventory(player);
            Console.WriteLine();
            Console.WriteLine("Press enter to continue");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("You made it past the guard. It felt good to fight again. Natural even. You could get the hang of this fast, and you knew it. There was a feeling deep in your gut and you just knew. You slowly keep walking.");
            Console.WriteLine();
            Console.WriteLine("Alert.");
            Console.WriteLine("Ready");
            Console.WriteLine();
            Console.WriteLine("Up ahead you can see the hall turn and know something's waiting for you just around the bend. You draw your weapon and charge forward.");
            Console.WriteLine();
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
            FightLoop(player, 4);
        }

        public static void TextParser(Player _player, string _input)
        {
            string input = _input;
            Player player = _player;

            switch (input)
            {
                case "inventory":
                    Item.DisplayInventory(player);
                    break;
                case "stats":
                    player.DisplayStats();
                    break;
            }
        }
         
        public static void Fight(Player _player, Enemy _enemy)
        {
            Player player = _player;
            Enemy enemy = _enemy;
            Random r = new Random();
            player.playerLevel = Math.Pow(player.playerXP/8, 0.3);

            Console.WriteLine("You are fighting a {0}. What do you do?", enemy.enemyName);
            do
            {
                double playerAttack = 0;
                string response = "";
                switch(player.playerClass)
                {
                    case "mage":
                        Console.WriteLine();
                        Console.WriteLine("1. Attack with weapon");
                        Console.WriteLine("2. Cast Spell");
                        Console.WriteLine("3. Use Item");
                        Console.WriteLine();
                        Console.Write("Enter your selection: ");
                        response = Console.ReadLine().ToLower();
                        TextParser(player, response);
                        switch (response)
                        {
                            case "1":
                            case "attack":
                                playerAttack = Math.Ceiling((Math.Pow(player.playerMysticism, 0.5) + r.Next((int)Math.Ceiling(player.activeWeapon.minStat), (int)Math.Ceiling(player.activeWeapon.maxStat)))+ player.attackBonus);
                                break;
                            case "2":
                            case "cast":
                                playerAttack = SpellLoop(player, response);
                                break;
                            case "3":
                            case "use":
                                Item.UseItem(player);
                                continue;
                                break;
                            case "stats":
                            case "inventory":
                                continue;
                                break;
                        }
                        break;
                    case "fighter":
                        Console.WriteLine();
                        Console.WriteLine("1. Attack with weapon");
                        Console.WriteLine("2. Power attack");
                        Console.WriteLine("3. Use Item");
                        Console.WriteLine();
                        Console.Write("Enter your selection: ");
                        response = Console.ReadLine().ToLower();
                        TextParser(player, response);
                        switch (response)
                        {
                            case "1":
                            case "attack":
                                playerAttack = Math.Ceiling((Math.Pow(player.playerStrength, 0.5) + r.Next((int)Math.Ceiling(player.activeWeapon.minStat), (int)Math.Ceiling(player.activeWeapon.maxStat)))+ player.attackBonus);
                                break;
                            case "2":
                            case "power":
                                playerAttack = SpellLoop(player,response);
                                break;
                            case "3":
                            case "use":
                                Item.UseItem(player);
                                continue;
                                break;
                            case "stats":
                            case "inventory":
                                continue;
                                break;
                        }
                        break;
                    case "rogue":
                        Console.WriteLine();
                        Console.WriteLine("1. Attack with weapon");
                        Console.WriteLine("2. Sneak attack");
                        Console.WriteLine("3. Use Item");
                        Console.WriteLine();
                        Console.Write("Enter your selection: ");
                        response = Console.ReadLine().ToLower();
                        TextParser(player, response);
                        switch (response)
                        {
                            case "1":
                            case "attack":
                                playerAttack = Math.Ceiling((Math.Pow(player.playerDexterity, 0.5) + r.Next((int)Math.Ceiling(player.activeWeapon.minStat), (int)Math.Ceiling(player.activeWeapon.maxStat)))+ player.attackBonus);
                                break;
                            case "2":
                            case "sneak":
                                playerAttack = SpellLoop(player, response);
                                break;
                            case "3":
                            case "use":
                                Item.UseItem(player);
                                continue;
                                break;
                            case "stats":
                            case "inventory":
                                continue;
                                break;
                        }
                        break;
                }
                enemy.enemyHP -= playerAttack;
                Console.WriteLine();
                Console.WriteLine("You hit for {0} damage. Enemy has {1} HP left. \nYou have {2} MP left.", (int)Math.Ceiling(playerAttack), (int)Math.Ceiling(enemy.enemyHP), (int)Math.Ceiling(player.playerMP));
                if(enemy.enemyHP <= 0)
                {
                    double item1id = 0;
                    double item1pct = 0;
                    double item2id = 0;
                    double item2pct = 0;
                    double item3id = 0;
                    double item3pct = 0;
                    string itemname = "";

                    Console.WriteLine("You win!");
                    Console.WriteLine("You get {0} XP.", enemy.enemyXP);
                    player.playerXP += enemy.enemyXP;
                    player.playerGold +=enemy.enemyGold;
                    string sql = "select * from enemies where enemies.EnemyID = @EnemyID";
                    SQLiteConnection gamedb = new SQLiteConnection("Data Source=gamedata.sqlite; Version=3;");
                    gamedb.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
                    {
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@EnemyID", enemy.enemyID);
                        SQLiteDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            item1id = (Int64)reader["Item1"];
                            item1pct = (Int64)reader["Item1Pct"];
                            item2id = (Int64)reader["Item2"];
                            item2pct = (Int64)reader["Item2Pct"];
                            item3id = (Int64)reader["Item3"];
                            item3pct = (Int64)reader["Item3Pct"];
                        }
                    }
                    gamedb.Close();

                    double itemchance = r.Next(1, 100);
                    if (itemchance < (int)item1pct)
                    {
                        Item.AddItem(player, item1id);
                        sql = "select * from items where items.ItemID = @ItemId";
                        gamedb.Open();
                        using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
                        {
                            cmd.Prepare();
                            cmd.Parameters.AddWithValue("@ItemID", item1id);
                            SQLiteDataReader reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                itemname = (string)reader["ItemName"];
                            }
                        }
                        gamedb.Close();
                        Console.WriteLine("You get {0}", itemname);
                    }   
                    itemchance = r.Next(1, 100);
                    if (itemchance < (int)item2pct)
                    {
                        Item.AddItem(player, item2id);
                        sql = "select * from items where items.ItemID = @ItemId";
                        gamedb.Open();
                        using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
                        {
                            cmd.Prepare();
                            cmd.Parameters.AddWithValue("@ItemID", item2id);
                            SQLiteDataReader reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                itemname = (string)reader["ItemName"];
                            }
                        }
                        gamedb.Close();
                        Console.WriteLine("You get {0}", itemname);
                    }
                    itemchance = r.Next(1, 100);
                    if (itemchance < (int)item3pct)
                    {
                        Item.AddItem(player, item3id);
                        sql = "select * from items where items.ItemID = @ItemId";
                        gamedb.Open();
                        using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
                        {
                            cmd.Prepare();
                            cmd.Parameters.AddWithValue("@ItemID", item3id);
                            SQLiteDataReader reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                itemname = (string)reader["ItemName"];
                            }
                        }
                        gamedb.Close();
                        Console.WriteLine("You get {0}", itemname);
                    }
                    break;
                }
                player.playerHP -= enemy.enemyAttack;
                Console.WriteLine("You are hit for {0} damage. You have {1} HP left", (int)enemy.enemyAttack, (int)player.playerHP);
                if(player.playerHP <= 0)
                {
                    Console.WriteLine("You lose!");
                    Console.WriteLine("You have 0 HP left");
                    break;
                }
            }while(true);
        }

        public static double CastSpell(Player _player, int _spellID)
        {
            Player player = _player;
            double spellID = _spellID;
            double spellDamage = 0;
            Random r = new Random();
            string sql = "select * from spells where SpellID = @SpellID";
            SQLiteConnection gamedb = new SQLiteConnection("Data Source=gamedata.sqlite; Version=3;");
            gamedb.Open();
            using (SQLiteCommand cmd = new SQLiteCommand(sql, gamedb))
            {
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@SpellID", spellID);
                SQLiteDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                {
                    double damageMin = (Int64)reader["DamageMin"];
                    double damageMax = (Int64)reader["DamageMax"];
                    double mpCost = (Int64)reader["MPcost"];
                    spellDamage = r.Next((int)damageMin, (int)damageMax);
                    player.playerMP -= mpCost;
                }
            }
            gamedb.Close();
            if (player.playerMP > 0)
            {
                return spellDamage;
            }
            else
            {
                return 0;
            }
        }

        public static double SpellLoop(Player _player, string _response)
        {
            Player player = _player;
            string response = _response;
            double playerAttack = 0;
            Console.WriteLine();
            do
            {
                player.DisplaySpellbook();
                Console.WriteLine();
                Console.Write("Enter your selection(1-5): ");
                response = Console.ReadLine();
                TextParser(player, response);
                switch (response)
                {
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                        int cast = Convert.ToInt32(response);
                        playerAttack = CastSpell(player, player.spellbook[cast-1,1]);
                        break;
                    case "stats":
                    case "inventory":
                        continue;
                        break;                         
                }
                break;
            }while (true);
            return playerAttack;
        }

        public static void FightLoop(Player _player, int _number)
        {
            Player player = _player;
            int number = _number;
            Random r = new Random();
            for (int i = 0; i <= number; i++)
            {
                player.attackBonus = 0;
                Enemy enemy = new Enemy();
                int enemySpawner = r.Next(0,100);
                if (enemySpawner <= 60)
                {
                    enemy.SpawnEnemy(1);
                    Fight(player, enemy);
                }
                else
                {
                    enemy.SpawnEnemy(2);
                    Fight(player, enemy);
                }
                if (player.playerHP > 0)
                {
                    Console.WriteLine();
                    Console.WriteLine("Press enter to continue");
                    Console.ReadLine();
                    Console.Clear();
                    player.DisplayStats();
                    Console.WriteLine();
                    Console.WriteLine("Press enter to continue");
                    Console.ReadLine();
                    Console.Clear();
                    continue;
                }
                else
                {
                    return;
                }
            }
        }
    }
}